https://gitlab.com/imam.ggwp/sql

=========Soal 1 Membuat Database==================

create database myshop;

use myshop;

=========Soal 2 Membuat Table di Dalam Database=========

create table users(
id int(8) primary key auto_increment,
name varchar(255),
email varchar(255),
password varchar(255)
);

create table categories(
id int(8) primary key auto_increment,
name varchar(255)
);

create table items(
id int(8),
name varchar(255),
description varchar(255),
price integer,
stock integer,
category_id integer,
foreign key(category_id) references categories(id)
);

=========Soal 3 Memasukkan Data pada Table==================
insert into users(name, email, password)
values("John Doe", "john@doe.com", john123), ("Jane doe", "jane@doe.com", jenita123);

MariaDB [myshop]> insert into categories(name)
    -> values("gadget"), ("cloth"), ("men"), ("women"), ("branded")
    -> ;
Query OK, 5 rows affected (0.001 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+


MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+


MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(8)       | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.013 sec)

MariaDB [myshop]> insert into items(name, description, price, stock, category_id)
    -> values("Sumsang b50", "hape keren dari merk sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur", 2000000, 10, 1)
    -> ;


MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang  | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur    | 2000000 |    10 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+


=========Soal 4 Mengambil Data dari Database=========

a. Mengambil data users
MariaDB [myshop]> select id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

b. Mengambil data items
MariaDB [myshop]> select * from items where  price > 1000000;
+----+-------------+------------------------------+---------+-------+-------------+
| id | name        | description                  | price   | stock | category_id |
+----+-------------+------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur   | 2000000 |    10 |           1 |
+----+-------------+------------------------------+---------+-------+-------------+

MariaDB [myshop]> select * from items where name like '%watch';
+----+------------+----------------------------+---------+-------+-------------+
| id | name       | description                | price   | stock | category_id |
+----+------------+----------------------------+---------+-------+-------------+
|  3 | IMHO Watch | jam tangan anak yang jujur | 2000000 |    10 |           1 |
+----+------------+----------------------------+---------+-------+-------------+



=========Soal 5 Mengubah Data dari Database=========

MariaDB [myshop]> update items set price = '2500000' where id = 1;
Query OK, 1 row affected (0.001 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang  | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur    | 2000000 |    10 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+





